var mysql = require('mysql');
var express = require('express');
var bodyParser = require('body-parser');
var multer = require('multer');
// import URL from '../Src/Config';

const connection = mysql.createConnection({
    host: 'localhost',
    user:'root',
    password: 'enginering',
    database: 'farmer'
});

connection.connect((err) => {
    if(err){
        throw err;
    }
    else 
        console.log('connected!!')
})


const app = express();
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())


var fileStorage = multer.diskStorage({ 
    destination: (req, file, cb) => {
        console.log("Body of multer :",file)
            cb(null, '../Images' )
    },
    filename: function (req, file, cb) {
        if (file.originalname) {
                cb(null, new Date().getTime() + '--' + file.originalname)
        }         
        else {
            cb(null, '')
        }
    }
});

var fileUploadDir = multer({ storage: fileStorage });

app.post('/insertImageUrl',fileUploadDir.any(),function(req,res){
    console.log("body of url:",req.body);
    let imgUrl = req.body.url;
    let query = `Insert into demo(PICTORA_IMG_URL) values('${imgUrl}')`
    connection.query(query, function(err,result){
        if(err) throw err;
        res.send(result);
    });
});

app.listen(3000,'192.168.43.252', () => {
    console.log('server is listening');
});