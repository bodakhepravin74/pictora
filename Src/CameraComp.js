import React,{Component} from 'react';
import {View,Text,Button,Image, Alert, ToastAndroid} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import NetInfo from "@react-native-community/netinfo";
import axios from 'axios';
import ImgPreview from './ImgPreview';
export default class CameraComp extends Component{
    constructor(props){
        super(props)
        this.state ={
            uri : '',
            connection_Status:''
        }
    }

    componentDidMount() {
        NetInfo.addEventListener(this.handleConnectivityChange);
      }
    
    //   componentWillUnmount() {
    //     NetInfo.removeEventListener(this.handleConnectivityChange);
    //   }
    
      handleConnectivityChange = state => {
        if (state.isConnected) {
          ToastAndroid.show('online',ToastAndroid.SHORT);
          this.setState({connection_Status: 'Online'});
        } else {
          ToastAndroid.show('You are not connected to the Internet!',ToastAndroid.SHORT);
          this.setState({connection_Status: 'Offline'});
        }
      };

    handlePicture=()=>{
        const options = {
            noData : true
        };
        ImagePicker.launchCamera(options,(response)=>{
            // ImagePicker.showImagePicker(options, (response) => {
                console.log('Response = ', response);          
                if (response.didCancel) {
                  console.log('User cancelled image picker');
                } else if (response.error) {
                  console.log('ImagePicker Error: ', response.error);
                } else if (response.customButton) {
                  console.log('User tapped custom button: ', response.customButton);
                } else {
                  const source = { uri: response.uri };
                  alert(JSON.stringify(source) )
                    // this.setState({uri:response.uri})
                    axios.post('http://192.168.43.252:3000/insertImageUrl', {
                        url: source
                      })
                      .then(function (response) {
                        console.log(response.data);
                        this.ImagePreview();
                      })
                      .catch(function (error) {
                        console.log(error);
                      });
                }
            //   });
        })
    }
    ImagePreview=()=>{
        <ImgPreview url={this.state.uri}/>
    }
    render(){
        return(
            <View style={{flex:1}}>
                {/* {this.handlePicture} */}
                {/* {<Image source={{uri:this.state.uri}} style={{height:100,width:200}} />} */}
                <Button title="Click Picture" onPress={this.handlePicture()}/>
            </View>

        )
    }
}