import React,{Component} from 'react';
import {View,Text,Button,StyleSheet} from 'react-native';



export default class Splash extends Component{
    render(){
        return(
            <View style={styles.container}>
                <Text style={{fontSize:25}}>Loading...</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,justifyContent:'center',alignItems:'center'
    }
})