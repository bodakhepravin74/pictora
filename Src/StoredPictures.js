import React,{Component} from 'react';
import {View,Text,Button,StyleSheet,Image,ToastAndroid} from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import URL from './Config';
import axios from 'axios'

export default class Splash extends Component{
    constructor(props){
     super(props);
     this.state = {
         arrayFile : [],
         uri:''
     }   
    }
    componentDidMount(){
        console.log('late:'+JSON.stringify( this.props))
        console.log(this.props.route.params.url)
        NetInfo.addEventListener(this.handleConnectivityChange);
    }
    handleConnectivityChange = state => {
        if (state.isConnected) {
            axios.post(`http://${URL}:3000/insertImageUrl`, {
                        url: JSON.stringify(this.props.route.params.url.uri)
                      })
                      .then(function (response) {
                        ToastAndroid.show("Picture has been saved Successfully",ToastAndroid.SHORT)
                      })
                      .catch(function (error) {
                        console.log(error);
                        ToastAndroid.show(error,ToastAndroid.SHORT)
                      });
            ToastAndroid.show("Picture has been saved Successfully",ToastAndroid.SHORT)
            this.props.navigation.navigate('Pictora')
        }
      };
    render(){
        return(
            <View style={styles.container}>
                <Image source={{uri:this.props.route.params.url.uri}} style={{height:200,width:200}}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,justifyContent:'center',alignItems:'center'
    }
})