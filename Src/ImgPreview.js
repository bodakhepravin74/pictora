import React from 'react';
import {
  Image,
  PixelRatio,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
  ToastAndroid
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import Geolocation from '@react-native-community/geolocation';
import NetInfo from "@react-native-community/netinfo";
import axios from 'axios';
import Splash from './Splash';
import URL from './Config';

export default class ImgPreview extends React.Component {
    componentDidMount(){
        setTimeout(()=>{
          this.setState({isLoading : false})
      },1000)
        Geolocation.getCurrentPosition(
            position => {
              const initialPosition = JSON.stringify(position.coords);
              this.setState({initialPosition});
            },
            error => Alert.alert('Error', JSON.stringify(error)),
            {enableHighAccuracy: true},
          );
          NetInfo.addEventListener(this.handleConnectivityChange);
    }
    //       componentWillUnmount() {
    //     NetInfo.removeEventListener(this.handleConnectivityChange);
    //   }
  state = {
    avatarSource: null,
    initialPosition :'',
    connection_Status:'',
    isLoading : true,
    imgData : ''
  };

  constructor(props) {
    super(props);
    this.selectPhotoTapped = this.selectPhotoTapped.bind(this);
  }

  handleConnectivityChange = state => {
    if (state.isConnected) {
      ToastAndroid.show('online',ToastAndroid.SHORT);
      this.setState({connection_Status: 'Online'});
      // this.props.navigation.navigate('Pictures')
    } else {
      ToastAndroid.show('You are not connected to the Internet!',ToastAndroid.SHORT);
      this.setState({connection_Status: 'Offline'});
      // this.props.navigation.navigate('Pictures')
    }
  };

  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.launchCamera(options, response => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        let source = {uri: response.uri};
        this.setState({data: source})
        this.setState({
          avatarSource: source,
        });
        if(this.state.connection_Status == 'Online'){
        axios.post(`http://${URL}:3000/insertImageUrl`, {
                        url: JSON.stringify(source)
                      })
                      .then(function (response) {
                        // console.log(response.data);
                        ToastAndroid.show("Picture has been saved Successfully",ToastAndroid.SHORT)
                        console.log(JSON.stringify(this.props))
                        // navigation.navigate('Pictures')
                      })
                      .catch(function (error) {
                        console.log(error);
                        ToastAndroid.show(error,ToastAndroid.SHORT)
                      });
        }else{
            ToastAndroid.show("You are offline, to save the picture please turn on the Internet!",ToastAndroid.SHORT)
                  this.props.navigation.navigate('Pictures',{url : source,connection_Status:this.state.connection_Status})
        }
      }
    });
  }

  render() {
    if(this.state.isLoading){
      return <Splash />
    }
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>
          <View
            style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
            {this.state.avatarSource === null ? (
              <Text>Click a Picture</Text>
            ) : (
                <View style={{flex:1}}>
              <Image style={styles.avatar} source={this.state.avatarSource} />
            <Text>{this.state.initialPosition}</Text>
              </View>
            )}
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    // borderRadius: 75,
    width: 200,
    height: 200,
  }
});