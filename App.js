import 'react-native-gesture-handler';
import React,{useState,useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';
import Splash from './Src/Splash';
import ImgPreview from './Src/ImgPreview';
import StoredPictures from './Src/StoredPictures';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

console.disableYellowBox = true;

const Stack = createStackNavigator();

function HomeScreen() {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Home Screen</Text>
    </View>
  );
}

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Pictora" component={ImgPreview} />
        <Stack.Screen name="Pictures" component={StoredPictures} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;